# SDLC Supporting Tools Extention Pack README

## Extensions

 This extentions pack will install and configure following extentions
 in order to set up an SDLC process environment for development. The aim 
 is to standardize code formatting and indentation style in source coding
 through out a project.

 This extension pack mainly comprises of code formatting and function
 documentation supporting tools as listed below:

1. C/C++ ( ms-vscode.cpptools )  - will support in formating the code
2. Doxygen Documentation Generator ( cschlosser.doxdocgen ) - will support
   in adding function documentation( function headaer comment, code comenting)
   in code.

Clang-formatter tool from LLVM project will be comming as part of c/c++ tool
manage the code formatting task and code comenting will be handled by using 
doxygen. When SDLC extension pack is installed, this will automatically generate 
necessary configuration files, do all required configurations for clang-formatter
and all the settings will be updated in settings.json. User need not do any kind 
of such configurations.

## How do format selected code using SDLC tool?

 The following gif image depics how do we install and use the SDLC tool to format
 and add function comment in the sourec code.

### Code Formatting

![Formatting](media/formatting.gif)
 
## How to document a function ?

In source code, on top of a function, type " /** " as shown below.

### Alignment

![Alignment](media/alignment.gif)


### Attributes

![Attributes](media/attributes.gif)

**Enjoy!**

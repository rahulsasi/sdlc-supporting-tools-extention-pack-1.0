"use strict";
/* This extention manages source code formatting and code commenting using clang-format and doxygen
 * clang-format binary will be comming along with cpp-tools extention. So we need to set the clang-formatter
 * path to cpp-tool formatter settings. Doxygen extention will provide support for standard code commenting.
 * Configurations and settings for all of these extensions are automatically being done when this extension
 * pack is installed. */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const vscode = __importStar(require("vscode"));
const path = __importStar(require("path"));
/* This method is called when your extension is activated
   Your extension is activated the very first time the command is executed*/
function activate(context) {
    /* Use the console to output diagnostic information (console.log) and errors (console.error).
       This line of code will only be executed once when your extension is activated */
    console.log('Congratulations, your extension "SDLC tools extension pack" is now active!');
    /* The code you place here will be executed every time your command is executed
       Display a message box to the user. */
    vscode.window.showInformationMessage('SDLC tools extention pack installed');
    /* Updating formatting settings in the cpptools extension for configuring clang-format */
    const cppTools = vscode.extensions.getExtension('ms-vscode.cpptools');
    const makefileTools = vscode.extensions.getExtension('ms-vscode.makefile-tools');
    const solarlint = vscode.extensions.getExtension('sonarsource.sonarlint-vscode');
    const configuration = vscode.workspace.getConfiguration();
    const target = vscode.ConfigurationTarget.Workspace; // Update the setting locally
    const overrideInLanguage = true; // Update the setting in the scope of the language
    // Disabling formatting on save option to make selected formatting to be done
    configuration.update('editor.formatOnSave', false, target, overrideInLanguage);
    /* Setting the format on save mode as modificationsIfAvailable in order to update
       only the modified line of code( requires source control). If no source control finds,
       format entire file */
    configuration.update('editor.formatOnSaveMode', 'modificationsIfAvailable', target, overrideInLanguage);
    /* Checking if cpptools extension is installed or not. If installed,
       do the foolowing confugurationd*/
    if (cppTools) {
        /*Setting the clang-format executable file path in the cpptool extention settings.*/
        const clangformatfilePath = path.join(cppTools.extensionPath, 'LLVM', 'bin', 'clang-format');
        /* The clang-format configuration file for the formatter tool clang-format */
        const clangformatconffilePath = path.join('file:', context.extensionPath, 'resources', '.clang-format');
        const cppToolsConfiguration = vscode.workspace.getConfiguration('C_Cpp');
        cppToolsConfiguration.update('intelliSenseEngine', 'default', vscode.ConfigurationTarget.Global);
        cppToolsConfiguration.update('formatting', 'clangFormat', vscode.ConfigurationTarget.Global);
        cppToolsConfiguration.update('clang_format_path', clangformatfilePath, vscode.ConfigurationTarget.Global);
        cppToolsConfiguration.update('clang_format_style', clangformatconffilePath, vscode.ConfigurationTarget.Global);
    }
    else {
        vscode.window.showInformationMessage('Failed to set up SDLC extension pack');
    }
}
exports.activate = activate;
// This method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map